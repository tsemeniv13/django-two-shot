from django.urls import path
from .views import user_log_in, logout_view, user_sign_up

urlpatterns = [
    path("login/", user_log_in, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", user_sign_up, name="signup"),
]
