from django.urls import path
from .views import (
    list_of_receipts,
    create_receipt,
    list_of_accounts,
    list_of_expense_categories,
    create_category,
    create_account,
)

urlpatterns = [
    path("", list_of_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", list_of_expense_categories, name="category_list"),
    path("accounts/", list_of_accounts, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
